package org.inventivetalent.lightlevel;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.inventivetalent.pluginannotations.PluginAnnotations;
import org.inventivetalent.pluginannotations.config.ConfigValue;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.UUID;

public class LightLevel extends JavaPlugin implements Listener {

	@ConfigValue(path = "radius")
	public static int radius = 8;
	@ConfigValue(path = "tools")
	public static String toolNameList = "TORCH";
	public static List<Material> tools;
	
	public static Set<UUID> tasks  = new HashSet<>();

	@Override
	public void onEnable() {
		Bukkit.getPluginManager().registerEvents(this, this);

		saveDefaultConfig();
		PluginAnnotations.CONFIG.loadValues(this, this);
		
		tools = new ArrayList<Material>();
		for(String toolName : Arrays.asList(toolNameList.split(","))){
			tools.add(Material.valueOf(toolName));
		}
	}

	@EventHandler
	public void onMove(PlayerMoveEvent event) {
		Player player = event.getPlayer();
		if (!player.isSneaking()) return;
		if(player.getInventory().getItemInHand() == null) return;
		if(tasks.contains(player.getUniqueId())) return;
		if(!validTool(player.getItemInHand().getType())) return;

		new ParticleTask(player).runTaskTimer(this, 0, 16);
		tasks.add(player.getUniqueId());
	}
	
	public static boolean validTool(Material tool){
	    return tools.contains(tool);
	}
}
